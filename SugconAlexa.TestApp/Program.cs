﻿
using System;
using System.Collections.Generic;
using System.Linq;
using SugconAlexa.Core.Data;
using SugconAlexa.Core.Services;
using SugconAlexa.Tests.Mocks;

namespace SugconAlexa.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
	        var result = new FeedContentService().ReadFeedContent();

            //"from": "2017-05-19T12:40:00.000Z",
            //"until": "2017-05-19T13:25:00.000Z",

            var mockDateTimeService = new MockDateTimeService(2018, 04, 23, 12, 00);

            var sessionService = new SessionService(result, mockDateTimeService);

            //var startDate = mockDateTimeService.UtcNow();

            //var sessions = sessionService.FindSessions(startDate).ToArray();

            //DumpSessions(sessions);

            //var nextSessions = sessionService.FindNextSessions(startDate).ToArray();

            //DumpSessions(nextSessions);

            //var todaysSessions = sessionService.TodaysSessions();

            //DumpSessions(todaysSessions);

            DumpSessions(sessionService.FindNextSessions(new DateTime(2018, 04, 23, 13, 20, 00)));

           // DumpSessions(sessionService.TodaysSessions());
        }

        private static void DumpSessions(IEnumerable<Session> sessions)
        {
            foreach (var session in sessions)
            {
                Console.WriteLine(session.Title + " " + session.From);
                Console.WriteLine("-------------");
            }

            Console.WriteLine("=====================");
        }
    }
}
