﻿using System;
using System.Linq;
using NUnit.Framework;
using SugconAlexa.Core.Services;

namespace SugconAlexa.Tests.Services
{
	[TestFixture]
	class SessionServiceTests
	{
		private const string UberModernApisSessionTitle = "UBER-MODERN APIS FOR SITECORE";
		private const string CommerceSessionTitle = "Using Commerce 9, SXA";
		private const string XConnectSessionTitle = "SITECORE XCONNECT & MARKETING";

		[Test]
		public void BeforeSession_CorrectCurrentSessionsAreReturned()
		{
			FindCurrentSessions(new DateTime(2018, 04, 23, 13, 20, 00), 0);
		}

		[Test]
		public void DuringSession_CorrectCurrentSessionsAreReturned()
		{
			FindCurrentSessions(new DateTime(2018, 04, 23, 14, 40, 00), 4, UberModernApisSessionTitle);

			FindCurrentSessions(new DateTime(2018, 04, 23, 15, 25, 00), 4, UberModernApisSessionTitle);
		}

		[Test]
		public void AfterSession_CorrectCurrentSessionsAreReturned()
		{
			FindCurrentSessions(new DateTime(2018, 04, 23, 15, 26, 00), 0);
		}

		[Test]
		public void BeforeSession_CorrectNextSessionsAreReturned()
		{
			FindNextSessions(new DateTime(2018, 04, 23, 13, 20, 00), 1, CommerceSessionTitle);
		}

		[Test]
		public void DuringSession_CorrectNextSessionsAreReturned()
		{
			FindNextSessions(new DateTime(2018, 04, 23, 14, 30, 00), 4, UberModernApisSessionTitle);

			FindNextSessions(new DateTime(2018, 04, 23, 14, 40, 00), 4, UberModernApisSessionTitle);

			FindNextSessions(new DateTime(2018, 04, 23, 15, 25, 00), 4, UberModernApisSessionTitle);
		}

		[Test]
		public void AfterSession_CorrectNextSessionsAreReturned()
		{
			FindNextSessions(new DateTime(2018, 04, 23, 15, 55, 00), 3, XConnectSessionTitle);
		}

		private static void FindCurrentSessions(DateTime dateTime, int expectedCount, string expectedSessionTitle = "")
		{
			var results = SessionService().FindSessions(dateTime);

			Console.WriteLine(dateTime);

			foreach (var result in results)
			{
				Console.WriteLine(result.Title);
			}

			Assert.That(results.Count(), Is.EqualTo(expectedCount));

			if (results.Any())
			{
				StringAssert.Contains(expectedSessionTitle.ToLower(), results.ElementAt(0).Title.ToLower());
			}

			Console.WriteLine("__________");
		}

		private static void FindNextSessions(DateTime dateTime, int expectedCount, string expectedSessionTitle)
		{
			var results = SessionService().FindNextSessions(dateTime);

			Console.WriteLine(dateTime);

			foreach (var result in results)
			{
				Console.WriteLine(result.Title);
			}

			Assert.That(results.Count(), Is.EqualTo(expectedCount));

			StringAssert.Contains(expectedSessionTitle.ToLower(), results.ElementAt(0).Title.ToLower());

			Console.WriteLine("__________");
		}

		private static SessionService SessionService()
		{
			var result = new FeedContentService().ReadFeedContent();

			var mockDateTimeService = new MockDateTimeService(2018, 04, 23, 12, 00);

			return new SessionService(result, mockDateTimeService);
		}
	}
}
