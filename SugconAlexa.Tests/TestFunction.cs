﻿using System;
using AlexaCore;
using AlexaCore.Content;
using Autofac;
using SugconAlexa.Core.Services;
using SugconAlexa.Tests.Mocks;

namespace SugconAlexa.Tests
{
    class TestFunction : Function
    {
        protected override void RegisterDependencies(ContainerBuilder builder, IntentParameters parameters)
        {
            base.RegisterDependencies(builder, parameters);

            builder.Register(a => new MockContentService()).As<IContentService<IntentContent>>();

            var feedContent = new FixedFeedContentService().ReadFeedContent();

            builder.Register(a => feedContent);

            builder.Register(a => feedContent.Rooms);

            var now = DateTime.UtcNow;

            builder.Register(a => new MockDateTimeService(2017, 05, 18, now.Hour, now.Minute)).As<IDateTimeService>();
        }
    }
}
