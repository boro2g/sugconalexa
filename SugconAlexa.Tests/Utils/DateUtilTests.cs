﻿using System;
using NUnit.Framework;
using SugconAlexa.Core.Services;
using SugconAlexa.Core.Utils;

namespace SugconAlexa.Tests.Utils
{
    [TestFixture]
    class DateUtilTests
    {
        [Test]
        [TestCase("1:00", 1, 00)]
        [TestCase("01:00", 1, 00)]
        [TestCase("10:00", 10, 00)]
        public void TimeParsesCorrectly(string input, int expectedHours, int expectedMinutes)
        {
            var result = new DateUtil().ParseAmazonTime(input);

            Assert.That(result.Hours, Is.EqualTo(expectedHours));

            Assert.That(result.Minutes, Is.EqualTo(expectedMinutes));
        }

        [Test]
        [TestCase("mo")]
        [TestCase("ab:00")]
        public void InvalidTime_Exceptions(string input)
        {
            Assert.Throws<FormatException>(() => new DateUtil().ParseAmazonTime(input));
        }

        [Test]
        [TestCase("saturday", DayOfWeek.Saturday)]
        [TestCase("sunday", DayOfWeek.Sunday)]
        [TestCase("monday", DayOfWeek.Monday)]
        [TestCase("tuesday", DayOfWeek.Tuesday)]
        [TestCase("wednesday", DayOfWeek.Wednesday)]
        [TestCase("thursday", DayOfWeek.Thursday)]
        [TestCase("friday", DayOfWeek.Friday)]
        [TestCase("today", DayOfWeek.Saturday)]
        [TestCase("tomorrow", DayOfWeek.Sunday)]
        [TestCase("yesterday", DayOfWeek.Friday)]
        public void DateParsesCorrectly(string input, DayOfWeek expected)
        {
            //saturday
            var mockDateTimeService = new MockDateTimeService(2018, 03, 10, 2, 2);

            var result = new DateUtil(mockDateTimeService).ParseDayOfWeek(input);

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase("rergre")]
        public void InvalidDate_Exceptions(string input)
        {
            //saturday
            var mockDateTimeService = new MockDateTimeService(2018, 03, 10, 2, 2);

            Assert.Throws<ArgumentException>(() => new DateUtil(mockDateTimeService).ParseDayOfWeek(input));
        }

        [Test]
        [TestCase("10:00", 10, 00, DayOfWeek.Saturday)]
        public void TimeOnDayParsesCorrectly(string time, int hour, int minutes, DayOfWeek dayOfWeek)
        {
            //saturday
            var mockDateTimeService = new MockDateTimeService(2018, 03, 10, 2, 2);

            var result = new DateUtil(mockDateTimeService).ParseAmazonDateTime(time);

            Assert.That(result.Hour, Is.EqualTo(hour));

            Assert.That(result.Minute, Is.EqualTo(minutes));

            Assert.That(result.DayOfWeek, Is.EqualTo(dayOfWeek));
        }

        [Test]
        [TestCase("10:00", "sunday", 10, 00, DayOfWeek.Sunday)]
        public void DateAndTimeOnDayParsesCorrectly(string time, string dayString, int hour, int minutes, DayOfWeek dayOfWeek)
        {
            //saturday
            var mockDateTimeService = new MockDateTimeService(2018, 03, 10, 2, 2);

            var dateUtil = new DateUtil(mockDateTimeService);

            var actualDayOfWeek = dateUtil.ParseDayOfWeek(dayString);
            
            Assert.That(actualDayOfWeek, Is.EqualTo(dayOfWeek));

            var result = dateUtil.ParseAmazonDateTime(time, actualDayOfWeek);

            Assert.That(result.Hour, Is.EqualTo(hour));

            Assert.That(result.Minute, Is.EqualTo(minutes));

            Assert.That(result.DayOfWeek, Is.EqualTo(dayOfWeek));
        }
    }
}
