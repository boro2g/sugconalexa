﻿using System.Collections.Generic;
using Alexa.NET.Request;
using NUnit.Framework;

namespace SugconAlexa.Tests.Intents
{
    [TestFixture]
    public class DayOfWeekAndTimeIntentTests
    {
        [Test]
        public void WhenTimeIsInvalid_ErrorMessageReturned()
        {
            var slots = new Dictionary<string, Slot> {{"time", new Slot {Value = "abc"}}};

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "I didn't understand the time: abc")
                .VerifyShouldEndSession(false);
        }

        [Test]
        public void WhenTimeIsMissing_ErrorMessageReturned()
        {
            var slots = new Dictionary<string, Slot>();

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "The time slot is missing")
                .VerifyShouldEndSession(false);
        }

        [Test]
        public void WhenDayIsInvalid_ErrorMessageReturned()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "abc"}},
                {"time", new Slot {Value = "12:00"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "I didn't understand the day: abc")
                .VerifyShouldEndSession(false);
        }

        [Test]
        public void WhenDayAndTimeAreValidAndSessionsMatch_SessionsAreReturned()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "friday"}},
                {"time", new Slot {Value = "13:35"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "at 13:35 on friday there are 4 sessions")
                .VerifyShouldEndSession(false)
                .RunAgain("YesIntent")
                .VerifyOutputSpeechValueContains(true, "main stage", "how to start selling product with sitecore", "room 1")
                .VerifyShouldEndSession(true);
        }

        [Test]
        public void WhenSessionIsUnderway_SessionsAreReturned()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "friday"}},
                {"time", new Slot {Value = "16:40"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "at 16:40 on friday there is 1 session")
                .RunAgain("YesIntent")
                .VerifyOutputSpeechValueContains(true, "main stage", "the sitecore philosophy")
                .VerifyShouldEndSession(true);
            ;
        }

        [Test]
        public void WhenDayAndTimeAreValidButNoSessionsMatch_NextSessionsAreReturned()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "friday"}},
                {"time", new Slot {Value = "12:00"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "There aren't any sessions at 12:00", "there are 4 sessions ")
                .VerifyShouldEndSession(false);
        }
        
        [Test]
        public void WhenNoMoreSessionsExist_TheSkillEnds()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "saturday"}},
                {"time", new Slot {Value = "12:00"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayAtTimeIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "there are no further sessions")
                .VerifyShouldEndSession(true);
        }
    }
}
