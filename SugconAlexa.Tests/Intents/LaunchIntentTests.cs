﻿using System.Collections.Generic;
using Alexa.NET.Request;
using NUnit.Framework;

namespace SugconAlexa.Tests.Intents
{
	[TestFixture]
    class LaunchIntentTests
	{
	    [Test]
	    public void BasicTest()
	    {
	        new TestRunner()
	            .RunInitialFunction("LaunchIntent")
	            .VerifyOutputSpeechValueContains(true, "welcome to sugcon")
	            .VerifyShouldEndSession(false);
	    }

	    [Test]
	    public void WhenAllIntentsAreRunWithoutSlots_NoErrorsAreRaised()
	    {
	        new TestRunner()
	            .RunInitialFunction("LaunchIntent")
	            .RunAllIntents();
	    }

	    [Test]
	    public void WhenAllIntentsAreRunWithRandomSlots_NoErrorsAreRaised()
	    {
	        new TestRunner()
	            .RunInitialFunction("LaunchIntent")
	            .RunAllIntents(new Dictionary<string, Slot>{{"TestSlot", new Slot()}});
	    }
    }
}
