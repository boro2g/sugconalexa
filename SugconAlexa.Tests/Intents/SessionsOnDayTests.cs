﻿using System.Collections.Generic;
using Alexa.NET.Request;
using NUnit.Framework;

namespace SugconAlexa.Tests.Intents
{
    [TestFixture]
    public class SessionsOnDayTests
    {
        [Test]
        public void WhatsSessionsAreOnFriday_CorrectInfo()
        {
            var slots = new Dictionary<string, Slot>
            {
                {"day", new Slot {Value = "friday"}}
            };

            new TestRunner()
                .RunInitialFunction("WhatsOnDayIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "On friday there are 23 sessions", "08:35")
                .VerifyShouldEndSession(false);
        }
    }
}
