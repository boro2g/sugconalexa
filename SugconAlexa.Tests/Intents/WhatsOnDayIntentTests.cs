﻿using System;
using System.Collections.Generic;
using System.Text;
using Alexa.NET.Request;
using NUnit.Framework;

namespace SugconAlexa.Tests.Intents
{
	[TestFixture]
    public class WhatsOnDayIntentTests
    {
        [Test]
        public void WhenSlotIsMissing_ValidResponseReturned()
        {
            TestWhatsOnDayWithSlots(null);
        }

        [Test]
        public void WhenSlotIsEmpty_ValidResponseReturned()
        {
            var slots = new Dictionary<string, Slot> { { "day", new Slot { Value = "" } } };

            TestWhatsOnDayWithSlots(slots);
        }

        [Test]
	    public void WhenSlotIsGarbage_ValidResponseReturned()
	    {
		    var slots = new Dictionary<string, Slot> { { "day", new Slot { Value = "banana" } } };

		    new TestRunner()
			    .RunInitialFunction("WhatsOnDayIntent", slots: slots)
			    .VerifyOutputSpeechValueContains(true, "I didn't understand the day: banana")
			    .VerifyShouldEndSession(false);
		}

        private static void TestWhatsOnDayWithSlots(Dictionary<string, Slot> slots)
        {
            new TestRunner()
                .RunInitialFunction("WhatsOnDayIntent", slots: slots)
                .VerifyOutputSpeechValueContains(true, "you need to specify a day - why not ask 'whats on monday'?")
                .VerifyShouldEndSession(false);
        }
    }
}
