﻿using NUnit.Framework;

namespace SugconAlexa.Tests.Intents
{
    [TestFixture]
    class SpeakerLotteryTests
    {
        [Test]
        public void WhenMoreSpeakersAreLoaded_SessionDoesntEnd()
        {
            new TestRunner()
                .RunInitialFunction("SpeakerLotteryIntent")
                .VerifyOutputIsNotEmpty()
                .VerifyShouldEndSession(false)
                .RunAgain("YesIntent")
                .VerifyShouldEndSession(false)
                .RunAgain("YesIntent")
                .VerifyShouldEndSession(false);
        }

	    [Test]
	    public void WhenNoSpeakersAreLeft_SessionDoesEnd()
	    {
		    new TestRunner()
			    .RunInitialFunction("SpeakerLotteryIntent")
			    .RunAgain("YesIntent", iterations:100)
			    .VerifyShouldEndSession(true);
	    }

        [Test]
        public void WhenNoMoreSpeakersAreRequested_HelpIsReturned()
        {
            new TestRunner()
                .RunInitialFunction("SpeakerLotteryIntent")
                .RunAgain("NoIntent")
                .VerifyShouldEndSession(false);
        }
    }
}
