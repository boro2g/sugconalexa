﻿using System;
using System.Linq;
using AlexaCore.Content;

namespace SugconAlexa.Tests.Mocks
{
    public class MockContentService : IContentService<IntentContent>
    {
        private string LoadAndFormatContent(string key, string defaultText, RequestParameters additionalRequestParameters,
            params string[] parameters)
        {
            return String.Format(defaultText, parameters);
        }

        ContentResponse IContentService<IntentContent>.LoadAndFormatContent(string key, string defaultText, RequestParameters additionalRequestParameters, params string[] parameters)
        {
            return new ContentResponse(new IntentContent() { ShouldEndSession = ShouldEndSession(key)}, key)
            {
                DefaultText = true,
                FormattedContent = LoadAndFormatContent(key,defaultText,additionalRequestParameters, parameters),
            };
        }

        private bool ShouldEndSession(string key)
        {
            var sessionsToEnd = new[] {"NoNextSessions", "SessionDetails"};

            if (sessionsToEnd.Contains(key))
            {
                return true;
            }
            
            return false;
        }
    }
}