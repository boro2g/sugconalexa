﻿using AlexaCore;
using AlexaCore.Testing;

namespace SugconAlexa.Tests
{
    public class TestRunner : AlexaCoreTestRunner<TestRunner>
    {
	    public override AlexaFunction BuildFunction()
	    {
			return new TestFunction();
		}
    }
}