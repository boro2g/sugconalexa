﻿namespace SugconAlexa.Core.Data
{
	public class Speaker
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Company { get; set; }
		public string Photo { get; set; }
	}
}