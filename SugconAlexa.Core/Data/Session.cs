﻿using System;

namespace SugconAlexa.Core.Data
{
	public class Session
	{
		public string Id { get; set; }
		public DateTime From { get; set; }
		public DateTime Until { get; set; }
		public string Room { get; set; }
		public string Title { get; set; }
		public string Summary { get; set; }
		public string Tags { get; set; }
		public SpeakerReference[] Speakers { get; set; }
	}
}