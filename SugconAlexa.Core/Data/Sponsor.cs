﻿using Newtonsoft.Json;

namespace SugconAlexa.Core.Data
{
	public class Sponsor
	{
		public string Id { get; set; }
		public string CompanyName { get; set; }
		[JsonProperty("companydescripion")]
		public string CompanyDescription { get; set; }
		public string CompanyLogo { get; set; }
		public string WebsiteUrl { get; set; }
		public string SponsorPackage { get; set; }
		public string SponsorType { get; set; }
	}
}