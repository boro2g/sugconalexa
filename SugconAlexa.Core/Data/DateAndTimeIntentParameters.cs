﻿using SugconAlexa.Core.Services;
using SugconAlexa.Core.Utils;

namespace SugconAlexa.Core.Data
{
    public class DateAndTimeIntentParameters
    {
        public ISessionService SessionService { get; }
        public SlotParser SlotParser { get; }
        public DateUtil DateUtil { get; }
        public FeedContent FeedContent { get; }

        public DateAndTimeIntentParameters(ISessionService sessionService, SlotParser slotParser, DateUtil dateUtil, FeedContent feedContent)
        {
            SessionService = sessionService;
            SlotParser = slotParser;
            DateUtil = dateUtil;
            FeedContent = feedContent;
        }
    }
}
