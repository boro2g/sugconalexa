﻿namespace SugconAlexa.Core.Data
{
	public class FeedContent
	{
		public Speaker[] Speakers {get; set; }
		public Session[] Sessions { get; set; }
		public Room[] Rooms { get; set; }
		public Sponsor[] Sponsors { get; set; }
	}
}
