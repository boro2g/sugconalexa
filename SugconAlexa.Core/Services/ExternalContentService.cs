﻿using System;
using System.Linq;
using System.Net;
using AlexaCore;
using AlexaCore.Content;

namespace SugconAlexa.Core.Services
{
    public class ExternalContentService : ContentService<IntentContent>
    {
        public ExternalContentService(PersistentQueue<ApplicationParameter> applicationParameters, string userId) 
            : base(applicationParameters, userId)
        {
        }

        public override string RequestUri(string intentKey, string userId, RequestParameters additionalRequestParameters)
        {
            string baseUrl = $"/sugconeurope/intents/{intentKey}?j=1&userId={userId}";

            if (additionalRequestParameters.Parameters.Any())
            {
                return
                    $"{baseUrl}{additionalRequestParameters.ToQueryString("&")}";
            }

            return baseUrl;
        }

        protected override IntentContent ProcessResult(IntentContent result)
        {
            result.Content = WebUtility.HtmlDecode(result.Content);

            return base.ProcessResult(result);
        }

        public override Uri BaseAddress => new Uri("http://alexacontent.boro2g.co.uk");
    }
}
