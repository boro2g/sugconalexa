﻿namespace SugconAlexa.Core.Services
{
    public class FixedFeedContentService : FeedContentService
    {
        public override string ResourceName()
        {
            return "SugconAlexa.Core.Data.sugconeurope2017schedule.json";
        }
    }
}
