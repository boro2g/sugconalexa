﻿using System;

namespace SugconAlexa.Core.Services
{
    public interface IDateTimeService
    {
        DateTime UtcNow();
    }

    public class DateTimeService : IDateTimeService
    {
        public DateTime UtcNow()
        {
            return DateTime.UtcNow;
        }

        public static DateTimeService Default => new DateTimeService();
    }

    public class MockDateTimeService : IDateTimeService
    {
        private readonly int _year;
        private readonly int _month;
        private readonly int _day;
        private readonly int _hours;
        private readonly int _minutes;

        public MockDateTimeService(int year, int month, int day, int hours = 0, int minutes = 0)
        {
            _year = year;
            _month = month;
            _day = day;
            _hours = hours;
            _minutes = minutes;
        }

        public DateTime UtcNow()
        {
            return new DateTime(_year, _month, _day, _hours, _minutes, 0);
        }
    }
}