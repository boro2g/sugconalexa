﻿using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using SugconAlexa.Core.Data;

namespace SugconAlexa.Core.Services
{
	public interface IFeedContentService
	{
		FeedContent ReadFeedContent();
	}

	public class FeedContentService : IFeedContentService
	{
	    public virtual string ResourceName()
	    {
	        return "SugconAlexa.Core.Data.sugconeurope2018schedule.json";
	    }

	    public FeedContent ReadFeedContent()
	    {
		    return JsonConvert.DeserializeObject<FeedContent>(ReadContent());
	    }

	    private string ReadContent()
	    {
		    var assembly = Assembly.GetAssembly(GetType());

		    var resourceName = ResourceName();

		    using (Stream stream = assembly.GetManifestResourceStream(resourceName))

		    using (StreamReader reader = new StreamReader(stream))
		    {
			    string result = reader.ReadToEnd();

			    return result;
		    }
		}
    }
}
