﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using SugconAlexa.Core.Data;

namespace SugconAlexa.Core.Services
{
    public interface ISessionService
    {
        IEnumerable<Session> TodaysSessions();
        IEnumerable<Session> TomorrowsSessions();
        IEnumerable<Session> TodaysRemainingSessions();
        IEnumerable<Session> FindSessions(DateTime startDateTime, DateTime? endDateTime = null);
        IEnumerable<Session> FindNextSessions(DateTime dateTime);
        IEnumerable<Session> SessionsOnDay(DayOfWeek dayOfWeek);
    }

    public class SessionService : ISessionService
    {
        private const int SessionLength = 45;

        private readonly FeedContent _feedContent;
        private readonly IDateTimeService _dateTimeService;

        public SessionService(FeedContent feedContent, IDateTimeService dateTimeService = null)
        {
            _feedContent = feedContent;
            _dateTimeService = dateTimeService ?? DateTimeService.Default;
        }

        public IEnumerable<Session> TodaysSessions()
        {
            var now = _dateTimeService.UtcNow();

            return _feedContent.Sessions.Where(a =>
                a.From.Year == now.Year && a.From.Month == now.Month && a.From.Day == now.Day)
                .OrderBy(a=>a.From);
        }

        public IEnumerable<Session> TomorrowsSessions()
        {
            var now = _dateTimeService.UtcNow().AddDays(1);

            return _feedContent.Sessions.Where(a =>
                    a.From.Year == now.Year && a.From.Month == now.Month && a.From.Day == now.Day)
                .OrderBy(a => a.From);
        }

        public IEnumerable<Session> TodaysRemainingSessions()
        {
            var now = _dateTimeService.UtcNow();

            return TodaysSessions().Where(a => a.Until > now);
        }

        public IEnumerable<Session> FindSessions(DateTime startDateTime, DateTime? endDateTime = null)
        {
            var currentSessions = _feedContent.Sessions.Where(a => startDateTime >= a.From && startDateTime <= a.Until);

            if (currentSessions.Any())
            {
                return currentSessions;
            }

            if (endDateTime == null)
            {
                endDateTime = startDateTime.AddMinutes(SessionLength);
            }

            return _feedContent.Sessions.Where(a => a.From >= startDateTime && a.Until <= endDateTime);
        }

        public IEnumerable<Session> FindNextSessions(DateTime dateTime)
        {
            var orderedSessions = _feedContent.Sessions.OrderBy(a => a.From).ToArray();

            var currentSessions = orderedSessions.Where(a => a.Until >= dateTime).ToArray();

            var sampleCurrentSession = currentSessions.FirstOrDefault();

            if (sampleCurrentSession != null)
            {
                if (sampleCurrentSession.Until >= dateTime)
                {
                    return new[] {sampleCurrentSession}.Union(currentSessions.Where(a => a.From == sampleCurrentSession.From));
                }

                var sampleNextSession = orderedSessions.FirstOrDefault(a => a.From >= sampleCurrentSession.From);

                if (sampleNextSession != null)
                {
                    return currentSessions.Where(a => a.From == sampleNextSession.From);
                }
            }

            return new Session[0];
        }

        public IEnumerable<Session> SessionsOnDay(DayOfWeek dayOfWeek)
        {
            var date = _dateTimeService.UtcNow();

            while (date.DayOfWeek != dayOfWeek)
            {
                date = date.AddDays(1);
            }

            return _feedContent.Sessions.Where(a =>
                    a.From.Year == date.Year && a.From.Month == date.Month && a.From.Day == date.Day)
                .OrderBy(a => a.From);
        }
    }
}
