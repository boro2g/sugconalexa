﻿using System;
using System.Collections.Generic;
using Alexa.NET.Request;

namespace SugconAlexa.Core.Utils
{
    public class SlotParser
    {
        private readonly DateUtil _dateUtil;

        public SlotParser(DateUtil dateUtil)
        {
            _dateUtil = dateUtil;
        }

        public string GetSlotValue(Dictionary<string, Slot> slots, string key, string defaultValue)
        {
            return slots.ContainsKey(key) ? slots[key].Value : defaultValue;
        }

        public TimeSpan? TimeSlot(Dictionary<string, Slot> slots, out string value)
        {
            value = GetSlotValue(slots, "time", "");

            try
            {
                var time = _dateUtil.ParseAmazonTime(value);

                return time;
            }
            catch (Exception)
            {
                Console.WriteLine($"Could not parse time slot: {value}");

                return null;
            }
        }

        public DayOfWeek? DayOfWeekSlot(Dictionary<string, Slot> slots, out string value)
        {
            value = GetSlotValue(slots, "day", "");

            try
            {
                var dayOfWeek = _dateUtil.ParseDayOfWeek(value);

                return dayOfWeek;
            }
            catch (Exception)
            {
                Console.WriteLine($"Could not parse day slot: {value}");

                return null;
            }
        }
    }
}
