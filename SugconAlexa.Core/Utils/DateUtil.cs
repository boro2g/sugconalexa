﻿using System;
using SugconAlexa.Core.Services;

namespace SugconAlexa.Core.Utils
{
    public class DateUtil
    {
        private readonly IDateTimeService _dateTimeService;

        public DateUtil(IDateTimeService dateTimeService = null)
        {
            if (dateTimeService == null)
            {
                dateTimeService = DateTimeService.Default;
            }

            _dateTimeService = dateTimeService;
        }

        public TimeSpan ParseAmazonTime(string value)
        {
            if (value.Contains(":"))
            {
                string[] parts = value.Split(":");

                if (parts.Length == 2)
                {
                    int hours = Int32.Parse(parts[0]);

                    int minutes = Int32.Parse(parts[1]);

                    return new TimeSpan(hours, minutes, 0);
                }
            }

            throw new FormatException($"Could not parse: {value}");
        }

        public DateTime ParseAmazonDateTime(string time)
        {
	        return ParseAmazonDateTime(ParseAmazonTime(time));
        }

	    public DateTime ParseAmazonDateTime(TimeSpan time)
	    {
		    var timeSpan = time;

		    var now = _dateTimeService.UtcNow();

		    return new DateTime(now.Year, now.Month, now.Day) + timeSpan;
	    }

        public DateTime ParseAmazonDateTime(string time, string dayOfWeek)
        {
            return ParseAmazonDateTime(ParseAmazonTime(time), ParseDayOfWeek(dayOfWeek));
        }

        public DateTime ParseAmazonDateTime(string time, DayOfWeek dayOfWeek)
		{
			return ParseAmazonDateTime(ParseAmazonTime(time), dayOfWeek);
		}

	    public DateTime ParseAmazonDateTime(TimeSpan time, DayOfWeek dayOfWeek)
	    {
		    if (dayOfWeek == _dateTimeService.UtcNow().DayOfWeek)
		    {
			    return ParseAmazonDateTime(time);
		    }

		    var dayInFuture = _dateTimeService.UtcNow();

		    while (dayOfWeek != dayInFuture.DayOfWeek)
		    {
			    dayInFuture = dayInFuture.AddDays(1);
		    }

		    return new DateTime(dayInFuture.Year, dayInFuture.Month, dayInFuture.Day) + time;
	    }

        public DayOfWeek Today()
        {
            return _dateTimeService.UtcNow().DayOfWeek;
        }

		public DayOfWeek ParseDayOfWeek(string dayOfWeek)
        {
            if (String.IsNullOrWhiteSpace(dayOfWeek))
            {
                throw new FormatException($"Could not parse: {dayOfWeek}");
            }

            if (String.Equals(dayOfWeek, "today", StringComparison.OrdinalIgnoreCase))
            {
                return _dateTimeService.UtcNow().DayOfWeek;
            }

            if (String.Equals(dayOfWeek, "tomorrow", StringComparison.OrdinalIgnoreCase))
            {
                return _dateTimeService.UtcNow().AddDays(1).DayOfWeek;
            }

            if (String.Equals(dayOfWeek, "yesterday", StringComparison.OrdinalIgnoreCase))
            {
                return _dateTimeService.UtcNow().AddDays(-1).DayOfWeek;
            }

            return Enum.Parse<DayOfWeek>(dayOfWeek, true);
        }
    }
}
