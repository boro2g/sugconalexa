﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using Newtonsoft.Json;
using SugconAlexa.Core.Data;

namespace SugconAlexa.Scraper
{
    class Program
    {
        static void Main(string[] args)
        {
            var rooms = LoadRooms();

            var speakers = LoadSpeakers();

            var sessions = LoadSessions(speakers);

            var sessionTimes = LoadSessionTimes(rooms);

            AssignTimesToSessions(sessions, sessionTimes);

            var feedContent = new FeedContent
            {
                Rooms = rooms,
                Sessions = sessions,
                Speakers = speakers,
                Sponsors = new Sponsor[0]
            };

            SaveFile(feedContent);
        }

        private static void SaveFile(FeedContent feedContent)
        {
            var outputFile = "D:\\Alexa\\SugconAlexa\\SugconAlexa.Core\\Data\\sugconeurope2018schedule.json";

            File.WriteAllText(outputFile, JsonConvert.SerializeObject(feedContent));
        }

        private static Session[] LoadSessionTimes(Room[] rooms)
        {
            var scheduleUrl = "http://www.sugcon.eu/schedule/";

            var web = new HtmlWeb();

            var doc = web.Load(scheduleUrl);

            var tables = doc.DocumentNode.SelectNodes("//*[contains(@class,'table-hover')]");

            DateTime startDay = new DateTime(2018, 04, 23);

            List<Session> sessions = new List<Session>();

            foreach (var table in tables)
            {
                foreach (var row in table.Descendants("tr"))
                {
                    string sessionName = row.ChildNodes[1].InnerText;

                    string speakers = row.ChildNodes[2].InnerText;

                    string time = row.ChildNodes[3].InnerText;

                    string room = row.ChildNodes[4].InnerText;

                    string[] timeParts = time.Split('-');

                    try
                    {
                        string[] startTimeParts = timeParts[0].Split(':');

                        DateTime startTime = startDay + new TimeSpan(int.Parse(startTimeParts[0]), int.Parse(startTimeParts[1]), 0);

                        string[] endTimeParts = timeParts[1].Split(':');

                        DateTime endTime = startDay + new TimeSpan(int.Parse(endTimeParts[0]), int.Parse(endTimeParts[1]), 0);

                        //Console.WriteLine($"{sessionName} {speakers} {startTime}-{endTime} {room}");

                        sessions.Add(new Session
                        {
                            Title = Clean(sessionName),
                            From = startTime,
                            Until = endTime,
                            Room = rooms.FirstOrDefault(a => a.Name == room).Id
                        });
                    }
                    catch (Exception)
                    {
                        Console.WriteLine($"Errored: {sessionName} {speakers}");
                    }
                }

                startDay = startDay.AddDays(1);
            }

            return sessions.ToArray();
        }

        private static void AssignTimesToSessions(Session[] sessions, Session[] sessionTimes)
        {
            Console.WriteLine("________________________________");

            int index = 0;

            foreach (var session in sessions)
            {
                var matchingSessionTime = sessionTimes.Where(a => TitlesMatch(a.Title, LookupTitle(session.Title)));

                if (matchingSessionTime.Count() > 1)
                {
                    Console.WriteLine($"Multiple names match: {session.Title}");
                }

                if (matchingSessionTime.FirstOrDefault() == null)
                {
                    Console.WriteLine($"Could not find session match for: {session.Title}");
                }

                var matchingSession = matchingSessionTime.FirstOrDefault();

                session.From = matchingSession.From;
                session.Until = matchingSession.Until;
                session.Room = matchingSession.Room;
                session.Id = index.ToString();
                index++;
            }
        }

        private static string LookupTitle(string sessionTitle)
        {
            if (sessionTitle == "New experience platform capabilities upcoming in Sitecore 9.1")
            {
                return "Opening presentation";
            }

            if (sessionTitle == "Sitecore Installation Framework")
            {
                return "SITECORE INSTALLATION FRAMEWORK (DEEPDIVE)";
            }

            if (sessionTitle == "Plug ins sitecore xperience commerce 9")
            {
                return "PLUG INS SITECORE EXPERIENCE COMMERCE 9";
            }

            return sessionTitle;
        }

        private static bool TitlesMatch(string title1, string title2)
        {
            title1 = RemoveSpecialCharacters(title1).ToLower();

            title2 = RemoveSpecialCharacters(title2).ToLower();

            return String.Equals(title1, title2);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_]+", "", RegexOptions.Compiled);
        }

        private static Room[] LoadRooms()
        {
            var scheduleUrl = "http://www.sugcon.eu/schedule/";

            var web = new HtmlWeb();

            var doc = web.Load(scheduleUrl);

            var tables = doc.DocumentNode.SelectNodes("//*[contains(@class,'table-hover')]");

            List<string> roomNames = new List<string>();

            foreach (var table in tables)
            {
                foreach (var row in table.Descendants("tr"))
                {
                    roomNames.Add(row.ChildNodes[4].InnerText);
                }
            }

            return roomNames.Distinct().Select((name, index) => new Room {Id = index.ToString(), Name = name}).ToArray();
        }

        private static Speaker[] LoadSpeakers()
        {
            var speakerUrl = "http://www.sugcon.eu/speakers/";

            var web = new HtmlWeb();

            var doc = web.Load(speakerUrl);

            var values = doc.DocumentNode.SelectNodes("//*[contains(@class,'speaker-title')]").Select(a => a.InnerText);

            return values.Select((speaker, index) => new Speaker {Id = index.ToString(), Name = Clean(speaker)}).ToArray();
        }

        private static Session[] LoadSessions(Speaker[] speakers)
        {
            var sessionUrl = "http://www.sugcon.eu/sessions/";

            var web = new HtmlWeb();

            var doc = web.Load(sessionUrl);

            var rows = doc.DocumentNode.SelectNodes("//*[contains(@class,'vc_row-fluid')]");

            List<Session> sessions = new List<Session>();

            foreach (var row in rows)
            {
                var speakersNames = row.Descendants().Where(a => a.HasClass("person-title")).Select(a => Clean(a.InnerText))
                    .ToArray();

                var titles = row.Descendants().Where(a => a.HasClass("style-title")).Select(a => a.InnerText).ToArray();

                var descriptions = row.Descendants().Where(a => a.Name == "p" && a.ParentNode.HasClass("wpb_wrapper")).Select(a => a.InnerText).ToArray();

                if (speakersNames.Any() && titles.Any() && descriptions.Any())
                {
                    sessions.Add(new Session
                    {
                        Title = Clean(titles.FirstOrDefault()),
                        Summary = Clean(descriptions.FirstOrDefault()),
                        Speakers = PairUpSpeakers(speakersNames, speakers)
                    });
                }
            }

            return sessions.ToArray();
        }

        private static SpeakerReference[] PairUpSpeakers(string[] speakersNames, Speaker[] speakers)
        {
            List <SpeakerReference> speakerReferences = new List<SpeakerReference>();

            foreach (var speakerName in speakersNames)
            {
                var matchingSpeaker = speakers.FirstOrDefault(a => TitlesMatch(a.Name, LookupSpeakerName(speakerName)));

                if (matchingSpeaker == null)
                {
                    Console.WriteLine($"Could not find speaker match for: {speakerName}");
                }
                else
                {
                    speakerReferences.Add(new SpeakerReference {Id = matchingSpeaker.Id});
                }
                
            }

            return speakerReferences.ToArray();
        }

        private static string LookupSpeakerName(string speakerName)
        {
            if (speakerName.ToUpper() == "ISHRAQ AL FATAFTAH")
            {
                return "ISHRAQ FATAFTAH";
            }

            return speakerName;
        }

        private static string Clean(string value)
        {
            return WebUtility.HtmlDecode(value);
        }
    }
}
