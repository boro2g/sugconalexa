using System;
using System.Linq;
using AlexaCore;
using AlexaCore.Content;
using AlexaCore.Intents;
using Amazon.Lambda.Core;
using Autofac;
using SugconAlexa.Core.Data;
using SugconAlexa.Core.Services;
using SugconAlexa.Core.Utils;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
namespace SugconAlexa
{
    public class Function : AlexaFunction
	{
		protected override IntentFactory IntentFactory()
		{
			return new FunctionIntentFactory();
		}

	    protected override void RegisterDependencies(ContainerBuilder builder, IntentParameters parameters)
	    {
	        var feedContent = new FeedContentService().ReadFeedContent();

	        builder.Register(a => feedContent);

	        builder.Register(a => feedContent.Rooms);

	        builder.Register(a => new ExternalContentService(parameters.ApplicationParameters, parameters.UserId))
	            .As<IContentService<IntentContent>>();

	        var now = DateTime.UtcNow;

	        var startDate = feedContent.Sessions.Min(a => a.From);

	        if (now.AddDays(6) < startDate)
	        {
	            builder.Register(a => new MockDateTimeService(2018, 04, 18, now.Hour, now.Minute)).As<IDateTimeService>();
	        }
	        else
	        {
	            builder.Register(a => new DateTimeService()).As<IDateTimeService>();
            }

	        builder.RegisterType<SessionService>().As<ISessionService>();

	        builder.RegisterType<DateUtil>();

	        builder.RegisterType<SlotParser>();

	        builder.RegisterType<DateAndTimeIntentParameters>();
	    }
    }
}
