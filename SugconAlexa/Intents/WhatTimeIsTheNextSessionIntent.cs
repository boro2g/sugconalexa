﻿using Alexa.NET.Response;
using SugconAlexa.Core.Data;
using SugconAlexa.Core.Services;
using SugconAlexa.Extensions;

namespace SugconAlexa.Intents
{
    public class WhatTimeIsTheNextSessionIntent : BaseWhatsOnNextIntent
	{
		public WhatTimeIsTheNextSessionIntent(ISessionService sessionService, IDateTimeService dateTimeService, FeedContent feedContent) 
			: base(sessionService, dateTimeService, feedContent)
		{
		}

		protected override SkillResponse NextSessionsToday(Session[] nextSessions)
		{
			var sampleNextSession = nextSessions[0];

			return this.TellAndFormatContent("TimeOfNextSessionsToday", "The next sessions start at {0}:{1}. Would you like more details?", null,
				sampleNextSession.From.Hour.ToString(), sampleNextSession.From.Minute.ToString());
		}
	}
}
