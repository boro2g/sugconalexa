﻿using SugconAlexa.Core.Data;

namespace SugconAlexa.Intents
{
    public class WhatsOnDayAtTimeIntent : BaseDayAndTimeIntent
    {
        public WhatsOnDayAtTimeIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters) : base(dateAndTimeIntentParameters)
        {
        }
    }
}