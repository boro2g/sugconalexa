﻿using System.Collections.Generic;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore.Intents;
using SugconAlexa.Extensions;

namespace SugconAlexa.Intents
{
    public class LaunchIntent : AlexaIntent
    {
	    protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
	    {
		    return this.TellAndFormatContent("LaunchIntent", $"Welcome to Sugcon. How can I help?");
	    }
    }
}
