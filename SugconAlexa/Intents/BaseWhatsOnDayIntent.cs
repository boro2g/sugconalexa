﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore.Intents;
using SugconAlexa.Core.Data;
using SugconAlexa.Extensions;
using Session = SugconAlexa.Core.Data.Session;

namespace SugconAlexa.Intents
{
    public class BaseWhatsOnDayIntent : AlexaIntent
    {
        protected readonly DateAndTimeIntentParameters DateAndTimeIntentParameters;

        public BaseWhatsOnDayIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters)
        {
            DateAndTimeIntentParameters = dateAndTimeIntentParameters;
        }

        protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
        {
            var sessions = LoadSessions(slots, out var dayOfWeek, out var daySlotValue).ToArray();

            if (dayOfWeek == null)
            {
                return ValidateDaySlot(daySlotValue);
            }

            return Reply(dayOfWeek.Value, sessions);
        }

        private SkillResponse ValidateDaySlot(string daySlotValue)
        {
            if (String.IsNullOrWhiteSpace(daySlotValue))
            {
                return this.TellAndFormatContent("DaySlotMissing",
                    $"You need to specify a day - why not ask 'Whats on Monday'?");
            }

            return this.TellAndFormatContent("CouldNotParseDay",
                "I didn't understand the day: {0}. Why not ask 'Whats on Monday'?", parameters: daySlotValue);
        }

        protected virtual SkillResponse Reply(DayOfWeek dayOfWeek, Session[] sessions)
        {
            if (sessions.Any())
            {
                return this.TellAndFormatContent("OnDayThereAreSessions",
                    "On {0} there are {1} sessions, starting at {2}. Why not ask what sessions are on at a specific time?",
                    null,
                    dayOfWeek.ToString(),
                    sessions.Length.ToString(),
                    sessions[0].StartTime()
                );
            }
            else
            {
                return this.TellAndFormatContent("NoSessionsToday", 
                    "Today there aren't any sessions. Why not ask about the specific days of the conference e.g. what's on at two pm on Monday?");
            }
        }

        protected virtual IEnumerable<Session> LoadSessions(Dictionary<string, Slot> slots, out DayOfWeek? dayOfWeek,
            out string daySlotValue)
        {
            dayOfWeek = DateAndTimeIntentParameters.SlotParser.DayOfWeekSlot(slots, out daySlotValue);

	        if (dayOfWeek.HasValue)
	        {
		        return DateAndTimeIntentParameters.SessionService.SessionsOnDay(dayOfWeek.Value).ToArray();
	        }

			return new Session[0];
        }
    }
}
