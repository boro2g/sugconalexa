﻿using SugconAlexa.Core.Data;
using SugconAlexa.Core.Services;

namespace SugconAlexa.Intents
{
    public class WhatsOnNextIntent : BaseWhatsOnNextIntent
	{
		public WhatsOnNextIntent(ISessionService sessionService, IDateTimeService dateTimeService, FeedContent feedContent) 
			: base(sessionService, dateTimeService, feedContent)
		{
		}
	}
}
