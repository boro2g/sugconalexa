﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore;
using AlexaCore.Intents;
using SugconAlexa.Core.Data;
using SugconAlexa.Core.Services;
using SugconAlexa.Extensions;
using Session = SugconAlexa.Core.Data.Session;

namespace SugconAlexa.Intents
{
    public class BaseWhatsOnNextIntent : IntentWithResponse
	{
	    public FeedContent FeedContent { get; }
	    protected readonly ISessionService SessionService;
		protected readonly IDateTimeService DateTimeService;

		public BaseWhatsOnNextIntent(ISessionService sessionService, IDateTimeService dateTimeService, FeedContent feedContent)
		{
		    FeedContent = feedContent;
		    SessionService = sessionService;
			DateTimeService = dateTimeService;
		}

		protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
		{
			var nextSessions = SessionService.FindNextSessions(DateTimeService.UtcNow()).ToArray();

			if (nextSessions.Any())
			{
				if (NextSessionIsAfterToday(nextSessions.ElementAt(0).From,DateTimeService.UtcNow()))
				{
					return NoMoreSessionsToday();
				}

				return NextSessionsToday(nextSessions);
			}

			return NoMoreSessionsEver();
		}

	    private bool NextSessionIsAfterToday(DateTime sessionDateTime, DateTime now)
	    {
	        return sessionDateTime.Date > now.Date;
	    }

	    protected virtual SkillResponse NextSessionsToday(Session[] nextSessions)
		{
		    if (nextSessions.Length == 1)
		    {
		        return this.TellAndFormatContent("NextSessionToday", "Next up today there is {0} session. Would you like more details?", null,
					nextSessions.Length.ToString());
            }

			return this.TellAndFormatContent("NextSessionsToday", "Next up today there are {0} sessions. Would you like more details?", null,
				nextSessions.Length.ToString());
		}

		protected virtual SkillResponse NoMoreSessionsToday()
		{
			return this.TellAndFormatContent("NoMoreSessionsToday", $"There are no more sessions today.");
		}

		protected virtual SkillResponse NoMoreSessionsEver()
		{
			return this.TellAndFormatContent("NoMoreSessions", "Sorry, there are no more sessions");
		}

		public override IEnumerable<PossibleResponse> PossibleResponses()
		{
			yield return PossibleResponse.YesResponse(YesResponse);
			yield return PossibleResponse.NoResponse(NoResponse);
		}

		private IntentResponse YesResponse()
		{
			var nextSessions = SessionService.FindNextSessions(DateTimeService.UtcNow()).ToArray();

		    ShouldEndSession = true;

            if (nextSessions.Length == 1)
		    {
		        return new IntentResponse(this.TellAndFormatContent("NextSessionDetail", "The next session is: {0}", null, nextSessions.BuildSessionInfoString(FeedContent)), ShouldEndSession);
            }

			return new IntentResponse(this.TellAndFormatContent("NextSessionDetails", "The next sessions are: {0}", null, nextSessions.BuildSessionInfoString(FeedContent)), ShouldEndSession);
		}

		private IntentResponse NoResponse()
		{
			ShouldEndSession = true;

		    return new IntentResponse(this.TellAndFormatContent("Goodbye", "Goodbye"), ShouldEndSession);
		}
	}
}
