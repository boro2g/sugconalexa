﻿using System.Collections.Generic;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore.Intents;
using SugconAlexa.Extensions;

namespace SugconAlexa.Intents
{
    public class HelpIntent : AlexaHelpIntent
    {
	    protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
	    {
	        ShouldEndSession = false;

	        return this.TellAndFormatContent("HelpIntent",
	            "Why not ask when the next session starts, or what sessions are on today?");
	    }
    }
}
