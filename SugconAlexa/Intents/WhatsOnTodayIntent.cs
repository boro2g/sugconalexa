﻿using System;
using System.Collections.Generic;
using Alexa.NET.Request;
using SugconAlexa.Core.Data;
using Session = SugconAlexa.Core.Data.Session;

namespace SugconAlexa.Intents
{
    public class WhatsOnTodayIntent : BaseWhatsOnDayIntent
    {
        public WhatsOnTodayIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters) : base(dateAndTimeIntentParameters)
        {
        }

        protected override IEnumerable<Session> LoadSessions(Dictionary<string, Slot> slots, out DayOfWeek? dayOfWeek, out string daySlotValue)
        {
            dayOfWeek = DateAndTimeIntentParameters.DateUtil.Today();

            daySlotValue = "today";

            return DateAndTimeIntentParameters.SessionService.TodaysSessions();
        }
    }
}