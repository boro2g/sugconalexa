﻿using System.Collections.Generic;
using System.Linq;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore;
using AlexaCore.Intents;

namespace SugconAlexa.Intents
{
    class YesIntent : IntentAsResponse
    {
        protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
        {
            var previousResponse = FindPreviousQuestionResponse(slots);

            ShouldEndSession = previousResponse.ShouldEndSession;

            return previousResponse.SkillResponse;
        }

        protected override CommandDefinition SelectLastCommand()
        {
            return Parameters.CommandQueue.Entries().LastOrDefault(a => a.ExpectsResponse);
        }
    }
}
