﻿using SugconAlexa.Core.Data;

namespace SugconAlexa.Intents
{
    public class WhatsOnDayIntent : BaseWhatsOnDayIntent
    {
        public WhatsOnDayIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters) : base(dateAndTimeIntentParameters)
        {
        }
    }
}