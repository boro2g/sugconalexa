﻿using SugconAlexa.Core.Data;

namespace SugconAlexa.Intents
{
    public class WhatsOnAtTimeIntent : BaseDayAndTimeIntent
    {
        public WhatsOnAtTimeIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters) : base(dateAndTimeIntentParameters)
        {
        }
    }
}
