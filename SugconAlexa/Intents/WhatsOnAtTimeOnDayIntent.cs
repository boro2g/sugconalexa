﻿using SugconAlexa.Core.Data;

namespace SugconAlexa.Intents
{
    public class WhatsOnAtTimeOnDayIntent : BaseDayAndTimeIntent
    {
        public WhatsOnAtTimeOnDayIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters) : base(dateAndTimeIntentParameters)
        {
        }
    }
}