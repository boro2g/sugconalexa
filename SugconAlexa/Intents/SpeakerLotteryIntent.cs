﻿using System.Collections.Generic;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore;
using AlexaCore.Extensions;
using AlexaCore.Intents;
using SugconAlexa.Core.Data;
using SugconAlexa.Extensions;

namespace SugconAlexa.Intents
{
    public class SpeakerLotteryIntent : IntentWithResponse
    {
        private readonly FeedContent _feedContent;
	    private readonly IntentFactory _intentFactory;

	    public SpeakerLotteryIntent(FeedContent feedContent, IntentFactory intentFactory)
	    {
		    _feedContent = feedContent;
		    _intentFactory = intentFactory;
	    }

	    protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
	    {
	        var speaker = FindNextSpeaker(_feedContent);

		    if (speaker == null)
		    {
			    return this.TellAndFormatContent("NoMoreSpeakersLotteryIntent", "The lottery is over", true);

		    }

	        return this.TellAndFormatContent("SpeakerLotteryIntent", "{0}", null, speaker.Name);
	    }

        private Speaker FindNextSpeaker(FeedContent feedContent)
        {
            var speaker = feedContent.Speakers.PickRandom();

            int count = 0;

            while (count < feedContent.Speakers.Length &&
                   Parameters.ApplicationParameters.SpeakerLogAlreadyContains(speaker.Name))
            {
                speaker = feedContent.Speakers.PickRandom();
	            count++;
            }

	        if (count == feedContent.Speakers.Length)
	        {
		        speaker = null;
	        }
	        else
	        {
				Parameters.ApplicationParameters.LogSpeaker(speaker.Name);
			}

            return speaker;
        }

		public override IEnumerable<PossibleResponse> PossibleResponses()
		{
			yield return PossibleResponse.YesResponse(() => new IntentResponse(GetResponseInternal(null), ShouldEndSession));
			yield return PossibleResponse.NoResponse(() => new IntentResponse(_intentFactory.HelpIntent().GetResponse(null), false));
		}
	}
}
