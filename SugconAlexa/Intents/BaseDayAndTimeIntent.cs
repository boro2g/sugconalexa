﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alexa.NET.Request;
using Alexa.NET.Response;
using AlexaCore;
using AlexaCore.Intents;
using SugconAlexa.Core.Data;
using SugconAlexa.Extensions;

namespace SugconAlexa.Intents
{
    public abstract class BaseDayAndTimeIntent : IntentWithResponse
    {
        private const string NextSessionsAtTime = "NextSessionsAtTime";
        private const string NextSessionAtTime = "NextSessionAtTime";
        private const string SessionsAtTime = "SessionsAtTime";
        private const string SessionAtTime = "SessionAtTime";

        public DateAndTimeIntentParameters DateAndTimeIntentParameters { get; }
        
        protected BaseDayAndTimeIntent(DateAndTimeIntentParameters dateAndTimeIntentParameters)
        {
            DateAndTimeIntentParameters = dateAndTimeIntentParameters;
        }

        protected override SkillResponse GetResponseInternal(Dictionary<string, Slot> slots)
        {
            TimeSpan? time = DateAndTimeIntentParameters.SlotParser.TimeSlot(slots, out var timeSlotValue);

            string examplePhrase = "Please try asking e.g. whats on monday at two pm?";

            if (time == null)
            {
                if (String.IsNullOrWhiteSpace(timeSlotValue))
                {
                    return this.TellAndFormatContent("TimeSlotMissing", "The time slot is missing. {0}", parameters: examplePhrase);
                }

                return this.TellAndFormatContent("CouldNotParseTime",
                    "I didn't understand the time: {0}. {1}", null, timeSlotValue, examplePhrase);
            }

            DayOfWeek? dayOfWeek = DateAndTimeIntentParameters.SlotParser.DayOfWeekSlot(slots, out var daySlotValue);

            if (dayOfWeek == null)
            {
                return this.TellAndFormatContent("CouldNotParseDay",
                    "I didn't understand the day: {0}. {1}", null, daySlotValue, examplePhrase);
            }

	        var dateTime = DateAndTimeIntentParameters.DateUtil.ParseAmazonDateTime(time.Value, dayOfWeek.Value);

            var sessions = DateAndTimeIntentParameters.SessionService.FindSessions(dateTime).ToArray();

            Parameters.ApplicationParameters.LogTime(timeSlotValue);

            Parameters.ApplicationParameters.LogDay(daySlotValue);

            string response = "";

            if (sessions.Any())
            {
                response = SessionAtTime;

                if (sessions.Length > 1)
                {
                    response = SessionsAtTime;
                }

                Parameters.ApplicationParameters.LogDateTimeState(response);

                if (sessions.Length > 1)
                {
                    return this.TellAndFormatContent(response, "At {0} on {1} there are {2} sessions. Would you like more details?", null,
						timeSlotValue, dayOfWeek.ToString(), sessions.Length.ToString());
                }

                return this.TellAndFormatContent(response, "At {0} on {1} there is {2} session. Would you like more details?", null,
					timeSlotValue, dayOfWeek.ToString(), sessions.Length.ToString());
            }

            var nextSessions = DateAndTimeIntentParameters.SessionService.FindNextSessions(dateTime).ToArray();

            if (nextSessions.Any())
            {
                response = NextSessionAtTime;

                if (nextSessions.Length > 1)
                {
                    response = NextSessionsAtTime;
                }

                Parameters.ApplicationParameters.LogDateTimeState(response);

                string nextSessionDay = "after";

                bool anotherDay = false;

                if (nextSessions[0].From.DayOfWeek != dateTime.DayOfWeek)
                {
                    nextSessionDay = "on another day";

                    anotherDay = true;
                }

                if (nextSessions.Length > 1 || anotherDay)
                {
                    return this.TellAndFormatContent(response, "There aren't any sessions at {0} however there are {1} sessions {2}. Would you like more details?", null,
						timeSlotValue, anotherDay ? "several" : nextSessions.Length.ToString(), nextSessionDay);
                }

                return this.TellAndFormatContent(response, "There aren't any sessions at {0} however there is {1} session {2}. Would you like more details?", null,
					timeSlotValue, nextSessions.Length.ToString(), nextSessionDay);
            }

            response = "NoNextSessions";
            
            Parameters.ApplicationParameters.LogDateTimeState(response);

            return this.TellAndFormatContent(response, "There are no further sessions. Goodbye", shouldEndSession: true);
        }

        public override IEnumerable<PossibleResponse> PossibleResponses()
        {
            yield return PossibleResponse.YesResponse(YesResponse);
            yield return PossibleResponse.NoResponse(NoResponse);
        }

        private IntentResponse YesResponse()
        {
            var dateTime = DateAndTimeIntentParameters.DateUtil.ParseAmazonDateTime(
                Parameters.ApplicationParameters.GetTime(), Parameters.ApplicationParameters.GetDay());

            var sessions = DateAndTimeIntentParameters.SessionService.FindSessions(dateTime).ToArray();

            var responseState = Parameters.ApplicationParameters.GetDateTimeState();

            if (String.Equals(responseState, NextSessionsAtTime) || String.Equals(responseState, NextSessionAtTime))
            {
                sessions = DateAndTimeIntentParameters.SessionService.FindNextSessions(dateTime).ToArray();
            }
            
            return new IntentResponse(this.TellAndFormatContent("SessionDetails", "{0}.", null, sessions.BuildSessionInfoString(DateAndTimeIntentParameters.FeedContent)), ShouldEndSession);
        }

        private IntentResponse NoResponse()
        {
            return new IntentResponse(this.TellAndFormatContent("Goodbye", "Goodbye"), ShouldEndSession);
        }
    }
}
