﻿using System.Linq;
using AlexaCore;
using Newtonsoft.Json;

namespace SugconAlexa.Extensions
{
    public static class PersistentQueueExtensions
    {
        private const string SpeakerLogApplicationParameter = "SpeakerLog";
        private const string SessionLogApplicationParameter = "SessionLog";
        private const string TimeApplicationParameter = "TimeLog";
        private const string DayApplicationParameter = "DayLog";
        private const string DateTimeStateApplicationParameter = "DateTimeState";

        public static void LogDateTimeState(this PersistentQueue<ApplicationParameter> queue, string value)
        {
            queue.AddOrUpdateValue(DateTimeStateApplicationParameter, value);
        }

        public static string GetDateTimeState(this PersistentQueue<ApplicationParameter> queue)
        {
            return queue.Find(a => a.Name == DateTimeStateApplicationParameter)?.Value;
        }

        public static void LogTime(this PersistentQueue<ApplicationParameter> queue, string time)
        {
            queue.AddOrUpdateValue(TimeApplicationParameter, time);
        }

        public static string GetTime(this PersistentQueue<ApplicationParameter> queue)
        {
            return queue.Find(a => a.Name == TimeApplicationParameter)?.Value;
        }

        public static void LogDay(this PersistentQueue<ApplicationParameter> queue, string day)
        {
            queue.AddOrUpdateValue(DayApplicationParameter, day);
        }

        public static string GetDay(this PersistentQueue<ApplicationParameter> queue)
        {
            return queue.Find(a => a.Name == DayApplicationParameter)?.Value;
        }

        public static void LogSpeaker(this PersistentQueue<ApplicationParameter> queue, string speaker)
        {
            LogStringIntoArray(queue, SpeakerLogApplicationParameter, speaker);
        }

        public static string[] GetSpeakerLog(this PersistentQueue<ApplicationParameter> queue)
        {
            return GetStrings(queue, SpeakerLogApplicationParameter);
        }

        public static bool SpeakerLogAlreadyContains(this PersistentQueue<ApplicationParameter> queue, string speaker)
        {
            return queue.GetSpeakerLog().Contains(speaker);
        }

        public static void LogSession(this PersistentQueue<ApplicationParameter> queue, string session)
        {
            LogStringIntoArray(queue, SessionLogApplicationParameter, session);
        }

        public static string[] GetSessionLog(this PersistentQueue<ApplicationParameter> queue)
        {
            return GetStrings(queue, SessionLogApplicationParameter);
        }

        public static bool SessionLogAlreadyContains(this PersistentQueue<ApplicationParameter> queue, string session)
        {
            return queue.GetSessionLog().Contains(session);
        }

        private static void LogStringIntoArray(PersistentQueue<ApplicationParameter> queue, string key, string value)
        {
            var existingLog = GetStrings(queue, key);

            queue.AddOrUpdateValue(key, JsonConvert.SerializeObject(existingLog.Concat(new[] { value }).Distinct()));
        }

        private static string[] GetStrings(PersistentQueue<ApplicationParameter> queue, string key)
        {
            var parameter = queue.Find(a => a.Name == key);

            return parameter == null ? new string[0] : JsonConvert.DeserializeObject<string[]>(parameter.Value);
        }
    }
}