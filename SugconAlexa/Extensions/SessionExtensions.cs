﻿using System.Collections.Generic;
using System.Linq;
using AlexaCore.Extensions;
using SugconAlexa.Core.Data;
using Session = SugconAlexa.Core.Data.Session;

namespace SugconAlexa.Extensions
{
    public static class SessionExtensions
    {
        public static string BuildSessionInfoString(this Session[] sessions, FeedContent feedContent)
        {
            return $"On {sessions[0].From.DayOfWeek} at {StartTime(sessions[0])} - {sessions.BuildSessionInfo(feedContent).JoinStringList(lastDivider:" and finally, ")}";
        }

        private static IEnumerable<string> BuildSessionInfo(this Session[] sessions, FeedContent feedContent)
	    {
		    foreach (var session in sessions.OrderBy(a => a.Room))
		    {
			    yield return session.BuildSessionInfo(feedContent);
		    }
	    }

	    public static string BuildSessionInfo(this Session session, FeedContent feedContent)
	    {
	        var matchingRoom = feedContent.Rooms.FirstOrDefault(a => a.Id == session.Room);

	        var matchingSpeakers = session.Speakers.SelectMany(a => feedContent.Speakers.Where(b => b.Id == a.Id))
	            .Select(a => a.Name);

		    return $"{matchingRoom.Name} - {matchingSpeakers.JoinStringList()} talk{(matchingSpeakers.Count() > 1 ? "" : "s")} about : {session.Title}.";
		}

        public static string StartTime(this Session session)
        {
            return $"{Pad(session.From.Hour)}:{Pad(session.From.Minute)}";
        }

        private static string Pad(int value)
        {
            string stringValue = value.ToString();

            return stringValue.Length == 1 ? $"0{stringValue}" : stringValue;
        }
    }
}
