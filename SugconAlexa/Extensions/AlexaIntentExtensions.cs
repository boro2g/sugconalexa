﻿using System;
using Alexa.NET.Response;
using AlexaCore;
using AlexaCore.Content;
using AlexaCore.Intents;
using Autofac;

namespace SugconAlexa.Extensions
{
    public static class AlexaIntentExtensions
    {
        public static SkillResponse TellAndFormatContent(this AlexaIntent intent, string key, string defaultText, bool? shouldEndSession = null,
            params string[] parameters)
        {
	        return intent.TellAndFormatContent(key, defaultText, null, shouldEndSession, parameters);
        }

        private static SkillResponse TellAndFormatContent(this AlexaIntent intent, string key, string defaultText,
            RequestParameters additionalRequestParameters, bool? shouldEndSession = null, params string[] parameters)
        {
            var content = Content().LoadAndFormatContent(key, defaultText, additionalRequestParameters, parameters);

			Console.WriteLine($"{intent.IntentName} - setting should end session: {content.RequestResponse.ShouldEndSession}");

	        if (shouldEndSession.HasValue)
	        {
		        intent.ShouldEndSession = shouldEndSession.Value;
	        }
	        else
	        {
				intent.ShouldEndSession = content.RequestResponse.ShouldEndSession;
			}

	        return intent.Tell(content.FormattedContent);
        }

        private static IContentService<IntentContent> Content()
        {
            return AlexaContext.Container.Resolve<IContentService<IntentContent>>();
        }
    }
}